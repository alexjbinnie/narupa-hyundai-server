# Copyright (c) University Of Bristol. All rights reserved.

import time

from nanoparticle import NanoparticleApplication

print("Starting University of Bristol-Hyundai Nanoparticle Collaboration Server")

app = NanoparticleApplication()

while True:
    time.sleep(0.1)
