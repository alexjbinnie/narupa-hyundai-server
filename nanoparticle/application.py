import time
import uuid
from concurrent.futures.thread import ThreadPoolExecutor
from queue import Queue
from threading import Lock
from typing import Optional

import ase
from narupa.app import NarupaFrameApplication
from narupa.app.app_server import start_default_server_and_discovery
from narupa.trajectory import FrameData
from narupa.utilities.change_buffers import DictionaryChange

from nanoparticle.nanoparticle import NanoparticleInstance
from .monte_carlo import grand_canconical_monte_carlo

DEFAULT_PORT = 54321


class NanoparticleApplication(NarupaFrameApplication):
    current_nanoparticle: Optional[NanoparticleInstance]
    current_energy: Optional[float]

    def __init__(self, port=DEFAULT_PORT):
        server, discovery = start_default_server_and_discovery(port=port)
        super().__init__(server, discovery=discovery, name="Narupa Hyundai")

        self.current_energy = None

        self.current_nanoparticle = None
        self.current_nanoparticle_lock = Lock()

        self.server.register_command('generate', self.generate_new_nanoparticle)
        self.server.register_command('export', self.export_current_nanoparticle_to_file)
        self.server.register_command('swap_particles', self.swap_particle_types)
        self.server.register_command('toggle_particle', self.toggle_particle_type)
        self.server.register_command('goto_snapshot', self.goto_snapshot)

        self.server.register_command('start_monte_carlo', self.start_monte_carlo)
        self.server.register_command('end_monte_carlo', self.end_monte_carlo)

        self.update_mp_state({
            "monte_carlo.temperature": 1.0
        })

        self._monte_carlo_in_progress = False
        self._energy_calculation_in_progress = True
        self._current_state_uuid = None

        self.generate_new_nanoparticle()

        executor = ThreadPoolExecutor(max_workers=3)
        executor.submit(self._monte_carlo_loop)
        executor.submit(self._energy_calculation_loop)
        executor.submit(self._send_frame_loop)

    def _mark_nanoparticle_as_dirty(self, energy_calc_needed=True):
        self._current_state_uuid = uuid.uuid4()

        if energy_calc_needed:
            self.current_energy = None
            self._energy_calculation_in_progress = True

        self.update_mp_state({
            "nanoparticle.parameters": {
                "layers": self.current_nanoparticle.layers,
                "count_element2": self.current_nanoparticle.count_element2,
                "element1": self.current_nanoparticle.element1,
                "element2": self.current_nanoparticle.element2
            }
        })

    def update_mp_state(self, dict):
        self.server.update_state("", DictionaryChange(
            updates=dict,
            removals=[]
        ))

    def remove_mp_state(self, *args):
        self.server.update_state("", DictionaryChange(
            updates={},
            removals=args
        ))

    def _send_frame_loop(self):
        last_uuid = None
        while True:
            if self._current_state_uuid != last_uuid:
                with self.current_nanoparticle_lock:
                    last_uuid = self._current_state_uuid
                    self.send_current_nanoparticle()

            time.sleep(0.1)

    def _energy_calculation_loop(self):
        while True:
            while not self._energy_calculation_in_progress:
                time.sleep(0.1)

            uuid = self._current_state_uuid
            name = self.current_nanoparticle.name

            print(f"Calculating energy for {name}")
            start = time.time()

            new_energy = self._calculate_energy()

            if uuid == self._current_state_uuid:
                elapsed = time.time() - start
                print(f"Calculated energy for {name} [took {elapsed:.2g} second(s)]")
                self.current_energy = new_energy
                self._energy_calculation_in_progress = False
                self.send_current_energy()

    def _calculate_energy(self):
        return self.current_nanoparticle.get_potential_energy()

    def _can_modify_nanoparticle(self):
        return not self._monte_carlo_in_progress

    def start_monte_carlo(self):
        self._monte_carlo_in_progress = True

    def end_monte_carlo(self):
        self._monte_carlo_in_progress = False

    def _monte_carlo_loop(self):
        """
        Background thread which waits until monte_carlo_in_progress is True, and then executes monte carlo
        until stopped
        :return:
        """
        while True:
            while not self._monte_carlo_in_progress:
                time.sleep(0.1)

            temperature_queue = Queue()
            temperature_queue.put_nowait(1)

            iterator = grand_canconical_monte_carlo(self.current_nanoparticle, temperature_queue)
            monte_carlo_attempts = 0
            monte_carlo_successes = 0

            self.update_mp_state({"monte_carlo": {
                "attempts": monte_carlo_attempts,
                "successes": monte_carlo_successes,
                "temperature": 1
            }})

            while self._monte_carlo_in_progress:
                try:
                    temperature_queue.put_nowait(self.server.copy_state()["monte_carlo.temperature"])
                    success, atoms, energy = next(iterator)
                except StopIteration:
                    break  # Iterator exhausted: stop the loop
                else:
                    if self._monte_carlo_in_progress:
                        monte_carlo_attempts += 1
                        if success:
                            monte_carlo_successes += 1
                            with self.current_nanoparticle_lock:
                                self.current_nanoparticle.atoms = atoms
                                self.current_energy = energy
                                self._mark_nanoparticle_as_dirty(energy_calc_needed=False)
                                self.send_current_energy()
                        print(
                            f"attempt: {monte_carlo_attempts}, success: {monte_carlo_successes}, energy: {energy:.3f}")
                        self.update_mp_state({"monte_carlo": {
                            "attempts": monte_carlo_attempts,
                            "successes": monte_carlo_successes
                        }})

            self.remove_mp_state("monte_carlo")

    def send_current_nanoparticle(self):
        frame_data = self.current_nanoparticle.get_frame_data()
        if self.current_energy is None:
            frame_data.potential_energy = float('nan')
        else:
            frame_data.potential_energy = self.current_energy
        print(f"Sending structure of {self.current_nanoparticle.name} to client(s)")
        self.frame_publisher.send_frame(1, frame_data)

    def send_current_energy(self):
        print(f"Sending energy {self.current_energy:.4g} eV to client(s)")

        frame_data = FrameData()
        frame_data.potential_energy = self.current_energy

        self.frame_publisher.send_frame(1, frame_data)

    def goto_snapshot(self, parameters, elements):
        self.generate_new_nanoparticle(elements=elements, **parameters)

    def generate_new_nanoparticle(self, count_element2=0, layers=2, element1='Pt', element2='Cu', elements=None):

        if not self._can_modify_nanoparticle():
            return

        print(
            f"Generating structure for {NanoparticleInstance.get_name(str(element1), str(element2), int(layers), int(count_element2))}")

        start = time.time()

        with self.current_nanoparticle_lock:
            self.current_nanoparticle = NanoparticleInstance(layers=int(layers), count_element2=int(count_element2),
                                                             element2=element2, elements=elements)
            self._mark_nanoparticle_as_dirty()

        elapsed = time.time() - start
        print(f"Generated structure for {self.current_nanoparticle.name} [took {elapsed:.2g} second(s)]")

    def export_current_nanoparticle_to_file(self):
        """
        Export the current nanoparticle to a timestamped XYZ file.
        """
        filename = time.strftime("%Y-%m-%d %H-%M-%S") + ".xyz"
        print(f"Exporting {self.current_nanoparticle.name} to file {filename}")
        with self.current_nanoparticle_lock:
            ase.io.write(filename, self.current_nanoparticle.atoms)

    def toggle_particle_type(self, particle_index=-1):

        if not self._can_modify_nanoparticle():
            return

        print(f"Switching the type of particle {int(particle_index)}")

        with self.current_nanoparticle_lock:
            self.current_nanoparticle.toggle_particle_type(int(particle_index))
            self._mark_nanoparticle_as_dirty()

    def swap_particle_types(self, particle_index1=-1, particle_index2=-1):

        if not self._can_modify_nanoparticle():
            return

        print(f"Swapping the types of particles {particle_index1} and {particle_index2}")

        with self.current_nanoparticle_lock:
            self.current_nanoparticle.swap_particles(int(particle_index1), int(particle_index2))
            self._mark_nanoparticle_as_dirty()
