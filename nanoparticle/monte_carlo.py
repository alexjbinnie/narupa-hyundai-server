import numpy as np
import random

from ase.units import kB
from ase.neighborlist import NeighborList

def grand_canconical_monte_carlo(nanoparticle, temperature_queue):
    """
    Swap neighbouring particles using GCMC
    """

    temperature = temperature_queue.get_nowait()

    initial_atoms = nanoparticle.atoms

    initial_particle_elements = initial_atoms.get_chemical_symbols()
    element_symbols = list(set(initial_particle_elements))
    indices_per_symbol = {element: [] for element in element_symbols}

    for particle_index, particle_element in enumerate(initial_particle_elements):
        indices_per_symbol[particle_element] += [particle_index]

    # Ensure the element_symbols is ordered so the first element has the lower concentration
    if len(indices_per_symbol[element_symbols[0]]) > len(indices_per_symbol[element_symbols[1]]):
        element_symbols.reverse()

    element1 = element_symbols[0]
    element2 = element_symbols[1]

    neighbour_cutoff = nanoparticle.atoms.get_distance(0, 1) / np.sqrt(2) / 1.5
    neighbour_list = NeighborList([neighbour_cutoff] * len(nanoparticle.atoms),
                      self_interaction=False,
                      bothways=True)

    current_atoms = initial_atoms
    current_energy = initial_atoms.get_potential_energy()

    while True:  # while success < steps:
        index_element1 = None
        index_element2 = None

        while index_element2 is None:
            # First, choose a random index from sym[0]
            random.shuffle(indices_per_symbol[element1])
            index_element1 = indices_per_symbol[element1][-1]

            # Calculate nearest neighbors
            neighbour_list.update(current_atoms)
            neighbouring_indices, _ = neighbour_list.get_neighbors(index_element1)

            # Determine if sym2 neighbors exist and choose one
            neighbours_which_are_element2 = [i for i in neighbouring_indices
                                    if current_atoms[i].symbol == element2]
            if neighbours_which_are_element2:
                index_element2 = random.sample(neighbours_which_are_element2, 1)[0]

        # Create new atoms object to test
        new_atoms = current_atoms.copy()
        new_atoms.set_calculator(current_atoms.get_calculator())

        # Update the atoms object by exchanging the two particles
        new_atoms[index_element1].symbol, new_atoms[index_element2].symbol = element2, element1

        # Calculate the energy of the new system
        new_energy = new_atoms.get_potential_energy()

        while not temperature_queue.empty():
            temperature = temperature_queue.get_nowait()

        if temperature <= 1/273.15:
            temperature = 1/273.15

        print(f"Monte carlo step at {273.15 * temperature} K")

        successful = new_energy < current_energy or np.exp(-(new_energy - current_energy) / (kB * 273.15 * temperature)) > np.random.rand()

        # Determine if lower than previous energy
        if successful:
            current_atoms = new_atoms
            current_energy = new_energy
            indices_per_symbol[element2][-1] = index_element1
            indices_per_symbol[element1][-1] = index_element2

        yield successful, current_atoms, current_energy