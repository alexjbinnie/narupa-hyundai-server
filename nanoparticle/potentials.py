from typing import Set

from amp import Amp

PtCu = Amp.load('./PtCu.amp')
PtFe = Amp.load('./PtFe_Large.amp')

def get_amp_potential(elements: Set[str]) -> Amp:
    """
    Get the AMP potential that can be used for the given set of atomic elements
    :param elements: A set of atomic symbols that the potential must support
    :return: A potential that can be used for these elements
    """
    if elements == {'Pt', 'Cu'}:
        return PtCu
    if elements == {'Pt', 'Fe'}:
        return PtFe
    raise ValueError(f"Unsupported elements {elements}")
