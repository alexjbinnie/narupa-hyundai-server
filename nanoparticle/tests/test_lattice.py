import pytest

from nanoparticle import get_lattice_constant


def lattice_pt(self):
    assert get_lattice_constant('Pt') == 3.82047


def lattice_cu(self):
    assert get_lattice_constant('Pt') == 3.61135


def lattice_fe(self):
    assert get_lattice_constant('Pt') == 3.473492


def lattice_unsupported(self):
    with pytest.raises(ValueError):
        get_lattice_constant('Md')