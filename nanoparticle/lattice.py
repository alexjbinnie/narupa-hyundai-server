from scipy.interpolate import interp1d

LATTICE_CONSTANTS = {
    'Pt': 3.82047,
    'Cu': 3.61135,
    'Fe': 3.473492
}


def get_lattice_constant_2elements(element1: str, element2: str, ratio: float) -> float:
    """
    Get the interpolated lattice constant for a lattice consisting of ratio amount of element1 and (1-ratio) amount of element2
    """
    lattice_constant1 = get_lattice_constant(element1)
    lattice_constant2 = get_lattice_constant(element2)
    return float(interp1d([0, 1], [lattice_constant1, lattice_constant2])(ratio))


def get_lattice_constant(element: str) -> float:
    if element in LATTICE_CONSTANTS:
        return LATTICE_CONSTANTS[element]
    raise ValueError(f"Unknown lattice constant for element {element}")
