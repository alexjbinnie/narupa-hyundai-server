import itertools
from typing import Optional, List

import ase
import numpy as np
from ase.cluster import FaceCenteredCubic
from narupa.ase import ase_to_frame_data
from narupa.trajectory.frame_data import POTENTIAL_ENERGY

from nanoparticle.potentials import get_amp_potential
from nanoparticle.lattice import get_lattice_constant_2elements


def signed_projection(u, v):
    return np.dot(u, v) / np.dot(v, v)


def project_cell_axis(axis):
    def func(v):
        return signed_projection(np.array(v), np.array(axis))

    return func


class NanoparticleInstance:
    """
    An instance of a specific nanoparticle, representing a specific layout of a set of atoms.
    """

    atoms: Optional[ase.Atoms]
    element1: str
    element2: str
    layers: int
    count_element2: int
    planeNames: List[str]
    lattice_constant: float

    def __init__(self, element1='Pt', element2='Cu', layers=2, count_element2=0, elements=None):
        self.atoms = None
        self.element1 = str(element1)
        self.element2 = str(element2)
        self.layers = int(layers)
        self.count_element2 = int(max(0, min(count_element2, self.get_atom_count_for_layers(self.layers))))
        self.planeNames = []
        self.planeIndices = []
        self.planeAxes = []
        self.lattice_constant = 1

        self.generate(elements)

        self.calculate_planes()

    def copy(self):
        copy = NanoparticleInstance()

        copy.atoms = self.atoms.copy()
        copy.atoms.set_calculator(self.atoms.get_calculator())

        copy.element1 = self.element1
        copy.element2 = self.element2
        copy.layers = self.layers
        copy.count_element2 = self.count_element2
        copy.planeNames = self.planeNames
        copy.planeIndices = self.planeIndices
        copy.planeAxes = self.planeAxes

        return copy

    @property
    def ratio(self):
        return self.count_element2 / self.get_atom_count_for_layers(self.layers)

    def generate(self, elements=None):

        print(elements)

        surfaces = [(1, 1, 1), (1, 0, 0)]
        layers = [self.layers, self.layers]

        self.lattice_constant = get_lattice_constant_2elements(self.element1, self.element2, self.ratio)

        self.atoms = FaceCenteredCubic(self.element1,
                                       surfaces,
                                       layers,
                                       vacuum=10.,
                                       latticeconstant=self.lattice_constant)

        self.atoms.center(about=(0., 0., 0.))
        atom_count = len(self.atoms)

        amp_calculator = get_amp_potential({self.element1, self.element2})
        self.atoms.set_calculator(amp_calculator)

        if elements is None:
            samp = np.random.choice(range(atom_count),
                                    self.count_element2,
                                    replace=False)

            for j in samp:
                self.atoms[j].symbol = self.element2
        else:
            for i in range(0, len(self.atoms)):
                self.atoms[i].symbol = elements[i]



    def calculate_planes(self):
        for plane in self.atoms.get_surfaces():
            if (abs(plane[0]) + abs(plane[1]) + abs(plane[2])) == 3:
                axis = self.lattice_constant * np.array(plane) / 3.0
            else:
                axis = self.lattice_constant * np.array(plane) / 2.0
            name = ""
            for i in range(0, 3):
                if plane[i] < 0:
                    name += "\u0305"
                name += str(abs(plane[i]))
            self.planeNames.append(name)
            self.planeAxes.append(0.1 * axis)
            self.planeIndices.append(list(map(project_cell_axis(axis), self.atoms.get_positions())))

    def toggle_particle_type(self, index):
        if index >= 0:
            if self.atoms[index].symbol == self.element1:
                self.atoms[index].symbol = self.element2
            else:
                self.atoms[index].symbol = self.element1

    def swap_particles(self, index1, index2):
        if index1 >= 0 and index2 >= 0:
            e1 = self.atoms[index1].symbol
            self.atoms[index1].symbol = self.atoms[index2].symbol
            self.atoms[index2].symbol = e1

    def get_potential_energy(self):
        return self.atoms.get_potential_energy()

    def get_frame_data(self):
        frame_data = ase_to_frame_data(self.atoms, topology=True, box_vectors=False, state=False, generate_bonds=False)
        frame_data.values[POTENTIAL_ENERGY] = float('nan')
        frame_data.arrays['plane.name'] = self.planeNames
        frame_data.arrays['plane.axis'] = list(itertools.chain.from_iterable(self.planeAxes))
        frame_data.arrays['plane.offset'] = list(itertools.chain.from_iterable(self.planeIndices))
        frame_data.values['lattice.constant'] = self.lattice_constant
        frame_data.values['layers'] = self.layers
        frame_data.values['element1'] = self.element1
        frame_data.values['element2'] = self.element2
        frame_data.values["count_element2"] = self.count_element2
        return frame_data

    @classmethod
    def get_name(cls, element1, element2, layers, count_element2):
        return f"{element1}({cls.get_atom_count_for_layers(layers)-count_element2}){element2}({count_element2})"

    @property
    def name(self):
        return self.get_name(self.element1, self.element2, self.layers, self.count_element2)

    @classmethod
    def get_atom_count_for_layers(cls, layers):
        """
        Get the number of atoms for a nanoparticle with the given number of layers
        """
        return (3 + 11 * layers + 15 * layers * layers + 10 * layers * layers * layers) // 3
